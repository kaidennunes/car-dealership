// CarDealership.cpp : Defines the entry point for the console application.
//

#include "Car.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

/*
1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
1

There is nothing in the inventory!

1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
2

The current balance is: $100000

1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
8

Goodbye




1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
1

There is nothing in the inventory!

1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
2

The current balance is: $100000

1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
8

Goodbye




1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
1

There is nothing in the inventory!

1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
2

The current balance is: $100000

1. Show Current Inventory
2. Show Current Balance
3. Buy a Car
4. Sell a Car
5. Paint a Car
6. Load File
7. Save File
8. Quit Program
8

Goodbye
*/
using namespace std;

int findIndexOfName(string name, vector<Car> inventory)
{
	int index = -1;
	for (int i = 0; i < inventory.size(); i++)
	{
		if (inventory[i].getName().compare(name) == 0)
		{
			index = i;
		}
	}
	return index;
}


int main()
{
	// Set our inventory
	vector<Car> inventory;
	// Set initial balance
	double balance = 10000.00;
	while (true)
	{
		cout << "1. Show Current Inventory" << endl;
		cout << "2. Show Current Balance" << endl;
		cout << "3. Buy a Car" << endl;
		cout << "4. Sell a Car" << endl;
		cout << "5. Paint a Car" << endl;
		cout << "6. Load File" << endl;
		cout << "7. Save File" << endl;
		cout << "8. Quit Program" << endl;
		int userInput;
		while (!(cin >> userInput))
		{
			cin.clear();
			cin.ignore(1000, '\n');
			cout << "\nPlease choose a valid menu option using its labeled number" << endl;
		}
		if (userInput == 1)
		{
			// Print out the inventory if there is any
			if (inventory.size() > 0)
			{
				for (int i = 0; i < inventory.size(); i++)
				{
					cout << "\n" << inventory[i].toString() << endl << endl;
				}
			}
			else
			{
				// Tell the user if there is nothing in the inventory
				cout << "\nThere is nothing in the inventory!" << endl << endl;
			}
		}
		else if (userInput == 2)
		{
			cout << "\nThe current balance is: $" << balance << endl << endl;
		}
		else if (userInput == 3)
		{
			string name;
			cout << "\nWhat is the name of the car?" << endl;
			cin >> name;

			// Check to make sure car is not already in inventory
			int index = findIndexOfName(name, inventory);
			if (index != -1)
			{
				cout << "\nThis name already belongs to another car" << endl << endl;
			}
			else
			{
				string color;
				cout << "\nWhat is the color of the car?" << endl;
				cin >> color;

				double price;
				cout << "\nWhat is the price of the car?" << endl;
				cin >> price;
				// Check to make sure price is not greater than balance
				if (price > balance)
				{
					cout << "\nThis car costs more than you have" << endl << endl;
				}
				else
				{
					balance -= price;
					Car addedCar(name, color, price);
					inventory.push_back(addedCar);
					cout << "\nThis car has been added to your inventory" << endl << endl;
				}
			}

		}
		else if (userInput == 4)
		{
			cout << "\nWhat car do you want to remove from the inventory?" << endl;
			string name;
			cin >> name;
			int index = findIndexOfName(name, inventory);
			if (index != -1)
			{
				balance += inventory[index].getPrice();
				inventory.erase(inventory.begin() + index);
				cout << "\nYour car has been sold!" << endl << endl;
			}
			else
			{

				cout << "\nThis car is not in the inventory" << endl << endl;
			}
		}
		else if (userInput == 5)
		{
			cout << "\nWhat car do you want to paint?" << endl;
			string name;
			cin >> name;
			int index = findIndexOfName(name, inventory);
			if (index != -1)
			{
				cout << "\nWhat color do you want to paint it?" << endl;
				string color;
				cin >> color;
				inventory[index].paint(color);
				cout << "\nYour car has been painted " << color << endl << endl;
			}
			else
			{
				cout << "\nThis car is not in the inventory" << endl << endl;
			}
		}
		else if (userInput == 6)
		{
			string fileName;
			cout << "\nWhat file do you want to open?" << endl << endl;
			cin >> fileName;

			string line;
			ifstream myfile(fileName + ".txt");
			if (myfile.is_open())
			{
				int offset = 0;
				while (getline(myfile, line))
				{
					if (offset == 0)
					{
						balance += stod(line);
					}
					else
					{
						string delimiter = " ";
						size_t pos = 0;
						string token;
						int position = 1;
						string name;
						string color;
						double price;
						while (position <= 3)
						{
							pos = line.find(delimiter);
							token = line.substr(0, pos);
							if (position == 1)
							{
								name = token;
							}
							else if (position == 2)
							{
								color = token;
							}
							else
							{
								price = stod(token);
							}
							line.erase(0, pos + 1);
							position++;
						}
						Car fileCar(name, color, price);
						inventory.push_back(fileCar);
					}
					offset++;
				}
				myfile.close();
				cout << "\nYour file has been added to the inventory" << endl << endl;
			}
			else
			{
				cout << "\nThe file could not be opened" << endl << endl;
			}
		}
		else if (userInput == 7)
		{
			if (inventory.size() < 0)
			{
				cout << "\nThere is nothing in the inventory!" << endl;
			}
			else
			{
				string fileName;
				cout << "\nWhat file do you want to save to?" << endl << endl;
				cin >> fileName;

				ofstream myfile;
				myfile.open(fileName + ".txt");
				myfile << balance << endl;
				for (int i = 0; i < inventory.size(); i++)
				{
					myfile << inventory[i].getName() << " " << inventory[i].getColor() << " " << inventory[i].getPrice() << endl;
				}
				myfile.close();
				cout << "\nYour file has been saved" << endl << endl;
			}
		}
		else if (userInput == 8)
		{
			cout << "\nGoodbye" << endl;
			break;
		}
		else {
			cout << "\nThat is not a menu option" << endl;
		}
	}
	system("pause");
	return 0;
}